<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Measurement;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees_all = Employee::all();
        $employees = array();
        foreach ($employees_all as $employee) {
            $measurement = Measurement::where('employee', $employee->id)->orderBy('created_at', 'DESC')->first();
            if ($measurement !== null) {
                $employee = (object) [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'measurement' => $measurement->reading,
                    'datetime' => $measurement->created_at,
                ];
            }
            else {
                $employee = (object) [
                    'id' => $employee->id,
                    'name' => $employee->name,
                    'measurement' => 'nėra',
                    'datetime' => 'nėra',
                ];
            }
            array_push($employees, $employee);
        }
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pin' => 'required|unique:employees|min:4|max:12'
        ]);
        if ($request->pin != $request->pin_confirm){
            $request->flash();
            return redirect()->back()->with('error', 'Neteisingai pakartotas PIN kodas!');
        }
        if (preg_match("[e-zE-Z|\W]", $request->pin)){
            $request->flash();
            return redirect()->back()->with('error', 'PIN kodas gali susidaryti tik iš skaitmenų ir raidžių A, B, C ir D!');
        }
        $employee = Employee::create([
            'PIN' => strtolower($request->pin),
            'name' => $request->name
        ]);
        $employee->save();
        return redirect('/employees')->with('success', 'Darbuotojas užregistruotas!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'old_pin' => 'required'
        ]);

        $employee = Employee::find($id);

        if ($request->old_pin != $employee->PIN){
            $request->flash();
            return redirect()->back()->with('error', 'Neteisingai įvestas senas PIN kodas!');
        } 
        if ($request->new_pin != $request->new_pin_confirm){
            $request->flash();
            return redirect()->back()->with('error', 'Neteisingai pakartotas naujasis PIN kodas!');
        }
        if (preg_match("[e-zE-Z|\W]", $request->new_pin)){
            $request->flash();
            return redirect()->back()->with('error', 'PIN kodas gali susidaryti tik iš skaitmenų ir raidžių A, B, C ir D!');
        }
        if(Employee::where('PIN', $request->new_pin)->count() > 0){
            $request->flash();
            return redirect()->back()->with('error', 'Naujasis PIN kodas jau yra priskirtas!');
        }

        $employee->PIN = $request->new_pin;
        $employee->save();

        return redirect('/employees')->with('success', 'Darbuotojo PIN kodas pakeistas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
