<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Measurement;
use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all()->pluck('name', 'id')->toArray();
        array_unshift($employees, 'Visi darbuotojai');
        return view('reports.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->date_from === 'Data nuo') {
            $request->flash();
            return redirect()->back()->with('error', 'Data nuo laukas yra privalomas');
        }
        if ($request->date_to === 'Data nuo') {
            $request->flash();
            return redirect()->back()->with('error', 'Data iki laukas yra privalomas');
        }
        if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $request->date_from)) {
            $request->flash();
            return redirect()->back()->with('error', 'Data nuo laukas įvestas neteisingai');
        }
        if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $request->date_to)) {
            $request->flash();
            return redirect()->back()->with('error', 'Data iki laukas įvestas neteisingai');
        }
        if (strcmp($request->date_from, $request->date_to) > 0) {
            $request->flash();
            return redirect()->back()->with('error', 'Data nuo turi būti ankstesnė už datą iki');
        }
        if (strtotime($request->date_from) > getdate()) {
            $request->flash();
            return redirect()->back()->with('error', 'Data nuo turi būti ne vėliau ' . date("Y-m-d"));
        }
        if (strtotime($request->date_to) > getdate()) {
            $request->flash();
            return redirect()->back()->with('error', 'Data iki turi būti ne vėliau ' . date("Y-m-d"));
        }
        $employees = null;
        $request->id == '0' ? $employees = Employee::all() : $employees = Employee::find($request->id);
        $report = null;
        if (is_a($employees, 'Illuminate\Database\Eloquent\Collection')) {
            $report = Report::create([
                'employee' => null,
                'date-from' => $request->date_from,
                'date-to' => $request->date_to,
            ]);
        } else {
            $report = Report::create([
                'employee' => $request->id,
                'date-from' => $request->date_from,
                'date-to' => $request->date_to,
            ]);
        }
        $report->save();
        $data = array();
        if (is_a($employees, 'Illuminate\Database\Eloquent\Collection')) {
            foreach ($employees as $employee) {
                $measurements = Measurement::where('employee', $employee->id)->orderBy('created_at', 'DESC')->whereBetween('created_at', [$request->date_from, $request->date_to])->get();
                foreach ($measurements as $measurement) {
                    if ($measurement !== null) {
                        $employee = (object) [
                            'id' => $employee->id,
                            'name' => $employee->name,
                            'measurement' => $measurement->reading,
                            'datetime' => $measurement->created_at,
                        ];
                    } else {
                        $employee = (object) [
                            'id' => $employee->id,
                            'name' => $employee->name,
                            'measurement' => 'nėra',
                            'datetime' => 'nėra',
                        ];
                    }
                    array_push($data, $employee);
                }
            }
        } else {
            $measurements = Measurement::where('employee', $employees->id)->orderBy('created_at', 'DESC')->whereBetween('created_at', [$request->date_from, $request->date_to])->get();
            foreach ($measurements as $measurement) {
                if ($measurement !== null) {
                    $employee = (object) [
                        'id' => $employees->id,
                        'name' => $employees->name,
                        'measurement' => $measurement->reading,
                        'datetime' => $measurement->created_at,
                    ];
                } else {
                    $employee = (object) [
                        'id' => $employees->id,
                        'name' => $employees->name,
                        'measurement' => 'nėra',
                        'datetime' => 'nėra',
                    ];
                }
                array_push($data, $employee);
            }
        }
        return view('reports.show', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $employees)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
