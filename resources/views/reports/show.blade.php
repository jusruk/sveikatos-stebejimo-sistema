@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ataskaita</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(empty($data))
                    <h2>Duomenų nėra!</h2>
                    @else
                     <table class="table table-striped">
                     <tr>
                     <th>Vardas</th>
                     <th>Matavimo rodmenys</th>
                     <th>Matavimo data</th>
                     </tr>
                     @foreach($data as $employee)
                     <tr>
                       <td>{{$employee->name}}</td>
                       @if($employee->measurement < 50)
                       <td>Visiškai blaivus</td>
                       @elseif($employee->measurement < 75)
                       <td>Leistina norma</td>
                       @elseif($employee->measurement < 100)
                       <td>Lengvas girtumas</td>
                       @else
                       <td>Sunkus girtumas</td>
                       @endif
                       <td>{{$employee->datetime}}</td>
                     </tr>
                     @endforeach
                     </table>
                     @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
