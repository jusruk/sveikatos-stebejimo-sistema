@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Kurti naują ataskaitą</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                     {!!Form::open(array('action' => 'ReportController@store', 'method' => 'post', 'class' => 'form'))!!}
                     <div>{!!Form::label('name', 'Darbuotojo vardas ir pavardė')!!}
                     {!!Form::select('id', $employees, 'Pasirinkite darbuotoją', ['class' => 'form-control'])!!}</div>
                     <div>{!!Form::label('date_from', 'Data nuo: ')!!}
                     {!!Form::text('date_from', 'Data nuo', array('id' => 'datefrom', 'class' => 'form-control'))!!}
                     {!!Form::label('date_to', 'Data iki: ')!!}
                     {!!Form::text('date_to', 'Data iki', array('id' => 'dateto', 'class' => 'form-control'))!!}
                     </div><br>
                     <div>{!!Form::submit('Sudaryti ataskaitą', ['class' => 'btn btn-primary'])!!}</div>
                     {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
