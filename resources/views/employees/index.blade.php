@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Darbuotojų sąrašas</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                     <table class="table table-striped">
                     <tr>
                     <th>Vardas</th>
                     <th>Paskutinio matavimo rodmenys</th>
                     <th>Matavimo data</th>
                     <th>Keisti PIN</th>
                     </tr>
                     @foreach($employees as $employee)
                     <tr>
                       <td>{{$employee->name}}</td>
                       @if($employee->measurement === 'nėra')
                       <td>{{$employee->measurement}}</td>
                       @elseif($employee->measurement < 50)
                       <td>Visiškai blaivus</td>
                       @elseif($employee->measurement < 75)
                       <td>Leistina norma</td>
                       @elseif($employee->measurement < 100)
                       <td>Lengvas girtumas</td>
                       @else
                       <td>Sunkus girtumas</td>
                       @endif
                       <td>{{$employee->datetime}}</td>
                       <td><a href="/employees/{{$employee->id}}/edit" class="btn btn-info">Keisti PIN</a></td>
                     </tr>
                     @endforeach
                     </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
