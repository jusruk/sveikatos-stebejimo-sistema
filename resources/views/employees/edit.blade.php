@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Keisti darbuotojo PIN kodą</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                     {!!Form::open(array('action' => ['EmployeeController@update', $employee->id], 'method' => 'post', 'class' => 'form'))!!}
                     <div>{!!Form::label('name', 'Darbuotojas')!!}</div>
                     <div>{!!Form::text('name', $employee->name, ['class' => 'form-control', 'readonly'])!!}</div>
                     <div>{!!Form::label('old_pin', 'Įveskite senąjį PIN kodą')!!}</div>
                     <div>{!!Form::text('old_pin', null, ['class' => 'form-control'])!!}</div>
                     <div>{!!Form::label('new_pin', 'Įveskite naująjį PIN kodą')!!}</div>
                     <div>{!!Form::text('new_pin', null, ['class' => 'form-control'])!!}</div>
                     <div>{!!Form::label('new_pin_confirm', 'Pakartokite naująjį PIN kodą')!!}</div>
                     <div>{!!Form::text('new_pin_confirm', null, ['class' => 'form-control'])!!}</div><br>
                     {!!Form::hidden('_method', 'PUT')!!}
                     <div>{!!Form::submit('Pakeisti PIN', ['class' => 'btn btn-primary'])!!}</div>
                     {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
