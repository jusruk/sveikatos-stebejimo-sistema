@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registruoti naują darbuotoją</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                     {!!Form::open(array('action' => 'EmployeeController@store', 'method' => 'post', 'class' => 'form'))!!}
                     <div>{!!Form::label('name', 'Darbuotojo vardas ir pavardė')!!}</div>
                     <div>{!!Form::text('name', null, ['class' => 'form-control'])!!}</div>                     
                     <div>{!!Form::label('pin', 'PIN kodas')!!}</div>
                     <div>{!!Form::text('pin', null, ['class' => 'form-control'])!!}</div>
                     <div>{!!Form::label('pin_confirm', 'Pakartokite PIN kodą')!!}</div>
                     <div>{!!Form::text('pin_confirm', null, ['class' => 'form-control'])!!}</div><br>
                     <div>{!!Form::submit('Registruoti darbuotoją', ['class' => 'btn btn-primary'])!!}</div>
                     {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
